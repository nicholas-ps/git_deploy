# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-24 23:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_3', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='diary',
            name='date',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
