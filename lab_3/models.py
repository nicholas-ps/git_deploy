from django.db import models

# Create your models here.
class Diary(models.Model):
	date = models.DateTimeField(auto_now_add=True)
	activity = models.TextField(max_length=60)
