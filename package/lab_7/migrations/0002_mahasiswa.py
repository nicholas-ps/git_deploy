# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-11-14 14:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_7', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Mahasiswa',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mahasiswa_nama', models.CharField(max_length=400)),
                ('npm', models.CharField(max_length=250)),
            ],
        ),
    ]
