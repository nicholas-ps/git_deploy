from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.forms.models import model_to_dict
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Friend, Mahasiswa
from package.api_csui_helper.csui_helper_lab_7 import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper()


def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada

    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    list_to_objects(mahasiswa_list)
    mahasiswa_list = Mahasiswa.objects.all()
    paginator = Paginator(mahasiswa_list, 25)
    page = request.GET.get('page')
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    #except EmptyPage:
        #data = paginator.page(paginator.num_pages)

    friend_list = Friend.objects.all()
    response = {"mahasiswa_list": data, "friend_list": friend_list}
    html = 'lab_7/lab_7.html'
    response["author"] = "Nicholas Priambodo"
    return render(request, html, response)

def list_to_objects(lst):
    for data in lst:
        nama = data['nama']
        npm = data['npm']
        Mahasiswa.objects.get_or_create(mahasiswa_nama=nama, npm=npm)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    response["author"] = "Nicholas Priambodo"
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        valid = isNpmValid(npm)
        data = {"isValid" : valid}

        if(data["isValid"]):
            friend = Friend(friend_name=name, npm=npm)
            friend.save()
            data['friend'] = model_to_dic(friend)

        return JsonResponse(data)

@csrf_exempt
def delete_friend(request):
    if request.method == "POST":
        friend_id = request.POST['id']
        Friend.objects.filter(id=friend_id).delete()
        data = {'id' : friend_id}
        return JsonResponse(data)

def isNpmValid(npm):
    friends = Friend.objects.all()

    for friend in friends:
        if friend.npm == npm:
            return False

    return True

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': not isNpmValid(npm) #lakukan pengecekan apakah Friend dgn npm tsb sudah ada
    }
    return JsonResponse(data)

def model_to_dic(obj):
    return model_to_dict(obj)

def daftar(request):
    friends = Friend.objects.all()
    lst = []

    for friend in friends:
        lst.append(model_to_dic(friend))

    daftar = {'list' : lst}
    return JsonResponse(daftar)
