from django.db import models
from datetime import datetime

# Create your models here.
class Message(models.Model):
    name = models.CharField(max_length=27)
    email = models.EmailField()
    message = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
    #created_date = models.DateTimeField(default=datetime.now, blank=True)


    def __str__(self):
        return self.message
